<?php
$servername = "localhost";
$username = "moodleuser";
$password = "password";
$dbname = "moodle";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

//1. Создать программу на PHP, которая будет выводить список курсов из moodle.
function getCourses($conn)
{
    $sql = "
select fullname course
from mdl_course
where category != 0
";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        ?>
        <table class="striped">
            <tr>
                <td>Course</td>
            </tr>
            <?php
            while ($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $row['course'] . "</td>";
                echo "</tr>";
            }
            ?>
        </table>
        <br>
        <?php
    }
}

//2. Написать скрипт, который находит всех пользователей с ролью студента и выводит их имена и фамилии.
function getStudents($conn)
{
    $sql = "
select firstname, lastname
from mdl_user mu
         join mdl_role_assignments mra on mra.userid = mu.id
where mra.roleid = 5
";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        ?>
        <table class="striped">
            <tr>
                <td>First name</td>
                <td>Last name</td>
            </tr>
            <?php
            while ($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $row['firstname'] . "</td>";
                echo "<td>" . $row['lastname'] . "</td>";
                echo "</tr>";
            }
            ?>
        </table>
        <br>

        <?php
    } else {
        echo "0 results";
    }
}

//6. Реализовать скрипт, который будет выводить самый популярный курс среди студентов.
function getHitCourse($conn)
{
    $sql = "
    with students as (
    select userid from mdl_role_assignments where roleid = 5
)
select mc.fullname course, count(mue.userid) students
from mdl_enrol me
         join mdl_user_enrolments mue on mue.enrolid = me.id
         join mdl_course mc on mc.id = me.courseid
where mue.userid in (select userid from students)
group by mue.enrolid
order by 2 desc
limit 1
";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        ?>
        <table class="striped">
            <tr>
                <td>Hit course</td>
            </tr>
            <?php
            while ($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $row['course'] . "</td>";
                echo "</tr>";
            }
            ?>
        </table>
        <br>

        <?php
    } else {
        echo "0 results";
    }
}



//7. Создать форму для добавления новых пользователей в moodle. Форма должна иметь поля для ввода имени, фамилии, email, пароля и роли.
?>


<!--<form method="POST" action="">-->
<!--    Username: <input type="text" name="username" id="username">-->
<!--    Password: <input type="text" name="password" id="password">-->
<!--    First name: <input type="text" name="firstname" id="firstname">-->
<!--    Last name: <input type="text" name="lastname" id="lastname">-->
<!--    Email: <input type="text" name="email" id="email">-->
<!--    Role: <select type="text" name="role" id="role">-->
<!--        <option value=1>manager</option>-->
<!--        <option value=2>course creator</option>-->
<!--        <option value=3>editing teacher</option>-->
<!--        <option value=4>teacher</option>-->
<!--        <option value=5>student</option>-->
<!--        <option value=6>guest</option>-->
<!--        <option value=7>user</option>-->
<!--        <option value=8>frontpage</option>-->
<!--    </select>-->
<!--    <input type="button" id="button" value="Submit"/>-->
<!--</form>-->
<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>-->
<!--    <script>-->
<!--        $(document).ready(function(){-->
<!---->
<!--            $('#button').on('click', function() {-->
<!--                const username = $('#username').val();-->
<!--                const password = $('#password').val();-->
<!--                const firstname = $('#firstname').val();-->
<!--                const lastname = $('#lastname').val();-->
<!--                const email = $('#email').val();-->
<!--                const role = $('#role').val();-->
<!---->
<!--                $.ajax({-->
<!--                    type: "POST",-->
<!--                    url: "./test_7_post.php",-->
<!--                    data: {-->
<!--                        username: username,-->
<!--                        password: password,-->
<!--                        firstname: firstname,-->
<!--                        lastname: lastname,-->
<!--                        email: email,-->
<!--                        role: role-->
<!--                    },-->
<!--                    success: function() {-->
<!--                        alert('yeeeeeeee');-->
<!--                    }-->
<!--                });-->
<!--            });-->
<!--        });-->
<!---->
<!--    </script>-->

<?php
//10. Написать скрипт, который будет выводить список пользователей и их результаты в тестах. Результаты должны быть отсортированы по убыванию.
function getUserGrades($conn)
{
    $sql = "
select concat(mu.firstname, ' ', mu.lastname) as name,
       mgg.finalgrade grade
from mdl_grade_grades mgg join mdl_user mu on mu.id = mgg.userid
where mgg.rawgrade is not null
order by 2 desc
";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        ?>
        <table class="striped">
            <tr>
                <td>Name</td>
                <td>Grade</td>
            </tr>
            <?php
            while ($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $row['name'] . "</td>";
                echo "<td>" . number_format((float)$row["grade"], 2, '.', '') . "</td>";
                echo "</tr>";
            }
            ?>
        </table>
        <br>

        <?php
    } else {
        echo "0 results";
    }
}

//4. Создать форму для загрузки файлов на сервер. Файлы должны быть загружены в каталог на сервере
// и должны быть доступны для скачивания через moodle.





//getCourses($conn);
//getStudents($conn);
//getHitCourse($conn);
//getUserGrades($conn);

$conn->close();
