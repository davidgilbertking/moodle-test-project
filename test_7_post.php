<?php
$db_servername = "localhost";
$db_username = "moodleuser";
$db_password = "password";
$db_dbname = "moodle";

// Create connection
$conn = new mysqli($db_servername, $db_username, $db_password, $db_dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$username = $_POST['username'];
$password = $_POST['password'];
$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$email = $_POST['email'];
$role = $_POST['role'];
$auth = 'manual';
$stmt = $conn->prepare("
insert into mdl_user (username, password, firstname, lastname, email, auth)
    values (?, ?, ?, ?, ?, ?)
    ");
$stmt->bind_param('ssssss', $username, $password, $firstname, $lastname, $email, $auth);
if ($stmt->execute() === TRUE) {
    echo "record inserted successfully";
} else {
    echo "Error: " . $stmt . "<br>" . $conn->error;
}
$id = mysqli_stmt_insert_id($stmt);
$time = time();
$stmt2 = $conn->prepare("
insert into mdl_role_assignments (roleid, userid, timemodified)
    values (?, ?, ?)
    ");
$stmt2->bind_param('isi', $role, $id, $time);
if ($stmt2->execute() === TRUE) {
    echo "record inserted successfully";
} else {
    echo "Error: " . $stmt2 . "<br>" . $conn->error;
}