<?php
/**
 * @throws Exception
 */
//1. Напишите программу на PHP, которая принимает на вход два числа, складывает их и выводит результат на экран.//

function addNumbers($x, $y): float
{
    if (is_numeric($x) && is_numeric($y)) {
        return $x + $y;
    } else {
        throw new Exception('I can add up numbers only');
    }
}

//2. Напишите функцию на PHP, которая определяет, является ли заданное число простым.
function isPrime(int $x): string
{
    $prime = "That's a prime number";
    $not_prime = "That's not a prime number";
    if ($x == 1)
        return $not_prime;
    for ($i = 2; $i <= $x / 2; $i++) {
        if ($x % $i == 0)
            return $not_prime;
    }
    return $prime;
}

//3. Напишите программу на PHP, которая принимает на вход строку и выводит ее на экран в обратном порядке.
strrev();
function reverseString(string $x): string
{
    $reversed = '';
    $length = strlen($x);
    for ($i = $length - 1; $i >= 0; $i--) {
        $reversed = $reversed . $x[$i];
    }
    return $reversed;
}

//4. Напишите функцию на PHP, которая принимает на вход массив чисел и возвращает среднее арифметическое его элементов.
/**
 * @throws Exception
 */
function averageArray(array $x): float
{
    foreach ($x as $element) {
        if (!is_numeric($element))
            throw new Exception('Array should contain numbers only');
        break;
    }
    if (count($x)) {
        return array_sum($x) / count($x);
    } else {
        throw new Exception('Array should not be empty');
    }
}

//5. Напишите программу на PHP, которая принимает на вход два массива чисел и выводит на экран элементы, которые встречаются в обоих массивах.
array_intersect();
function arrayIntersect(array $x, array $y): string
{
    $found_intersections = [];
    foreach ($x as $x_element) {
        if (in_array($x_element, $y)) {
            $found_intersections[] = $x_element;
        }
    }
    return implode(', ', $found_intersections);
}

//6. Напишите функцию на PHP, которая находит максимальный элемент в массиве чисел.
max();
/**
 * @throws Exception
 */
function maxArray(array $x): float
{
    foreach ($x as $x_element) {
        if (!is_numeric($x_element))
            throw new Exception('Array should not be empty');
        break;
    }
    rsort($x, 1);
    return $x[0];
}

//7. Напишите программу на PHP, которая принимает на вход строку и проверяет, является ли она палиндромом (читается одинаково справа налево и слева направо).
function isPalindrome(string $x): string
{
    $reverse = strrev($x);
    if ($x === $reverse) {
        return "This is a palindrome";
    } else {
        return "not a palindrome";
    }
}

//8. Напишите функцию на PHP, которая проверяет, является ли заданная строка валидным email адресом.
function checkEmailAddress(string $x): string
{
    if (filter_var($x, FILTER_VALIDATE_EMAIL)) {
        return "This is indeed an email";
    } else {
        return "Nope, not an email.";
    }
}

//9. Напишите программу на PHP, которая выводит на экран таблицу умножения от 1 до 10.
function multiplicationTable10()
{
    $x = $y = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    foreach ($x as $x_element) {
        foreach ($y as $y_element) {
            echo $x_element * $y_element . "\t";
        }
        echo "\r\n";
    }
}

//10. Напишите функцию на PHP, которая принимает на вход массив чисел и возвращает отсортированный по возрастанию массив.
sort();
function sortArrayAscending(array $x): array
{
    $y = $x;
    $sorted = [];
    $length = count($y);
    for ($i = $length; $i > 0; $i--) {
        if (!empty($y)) {
            $min = min($y);
            $sorted[] = $min;
            if (($key = array_search($min, $y)) !== false) {
                unset($y[$key]);
            }
        }
    }

    return $sorted;
}